# GeoFlex 2D - Terrain with Realtime Deformation

Versions: Unity **2022.3.4f1** and above

Introducing GeoFlex, your innovative solution for 2D terrain and geometry creation within Unity. Experience unparalleled control and responsiveness in terrain manipulation, as GeoFlex brings you endless opportunities for game design.

🟩 - complete, 🟨 - partially complete, 🟥 - to do  

Features:
- 🟩 **Infinite Terrain**: Enjoy limitless creativity with on-the-fly geometry chunk generation that seamlessly extends your landscape.
- 🟩 **Unity-Compatible 2D Physics**: Automatically integrates with unity physics and colliders, no extra work required.
- 🟩 **Dynamic Player-Centric Chunk Generation**: Generate and render only the terrain chunks needed - within a designated area around the player.
- 🟨 **Geometry Editor**: Sculpt and design your own unique environments right in the Unity editor.
- 🟩 **Rendering - Geometry Fill**: Generate meshes for rendering geometry fill.
- 🟥 **Rendering - Geometry Edges**: Generate meshes for rendering geometry edges.
- 🟥 **Customizable Procedural Generation**: Different noise types with parameters and support for adding custom noise for varied terrain features.
- 🟨 **Robust Data Management**: Save and load geometry scalar data efficiently at runtime.
- 🟥 **Multithreading Optimization**: Ensure swift performance and responsiveness with cutting-edge multithreading capabilities.
- 🟥 **Unity Asset Store Availability**: GeoFlex will be accessible through the Unity Asset Store, giving you streamlined access to the plugin.

GeoFlex's core is powered by an interpolated marching squares algorithm, extrapolating smooth polygon paths from scalar field data. The real-time efficiency of this algorithm lets you paint terrain into your scene in edit mode - or even destroy and build geometry within the game! Craft with the precision of a brush stroke, and explore endless possibilities with GeoFlex.

![Sample Video](/Media/Sample.mp4)