using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GeoFlex.Core;

namespace GeoFlex.Utility
{
	[AddComponentMenu("GeoFlex/Chunk Mesh Builder")]
	public class ChunkMeshBuilder : MonoBehaviour
	{
		private readonly Queue<MeshFilter> pool = new Queue<MeshFilter>();

		[SerializeField]
		private int prePoolCount = 15;

		private readonly Dictionary<GeometryChunk, MeshFilter> meshMap = new Dictionary<GeometryChunk, MeshFilter>();

		private GeometryController marchingSquares = null;

		// ----------------------------------------------------------------------------------------

		[SerializeField]
		public MeshFilter blueprint;

		[SerializeField]
		public Transform poolContainer = null;

		[SerializeField]
		public Transform meshContainer = null;

		[Tooltip(
			"Optional - if set, meshes will be attached  and removed automatically as chunks " +
			"come into and out of view of the camera chunk loader"
		)]
		public CameraChunkLoader cameraChunks = null;

		public GeometryController MarchingSquares {
			get {
				if (marchingSquares == null)
				{
					if(cameraChunks != null) marchingSquares = cameraChunks.MarchingSquares;
					else marchingSquares = GetComponent<GeometryController>();
				}
				return marchingSquares;
			}
			set { marchingSquares = value; }
		}

		// ----------------------------------------------------------------------------------------

		private MeshFilter GetPoolObject()
		{
			MeshFilter r;

			if (pool.Count > 0) r = pool.Dequeue();
			else
			{
				r = Instantiate(blueprint);
				r.name = "Chunk Mesh";
			}
			r.transform.SetParent(meshContainer, false);

			return r;
		}

		private void Recycle(MeshFilter mesh)
		{
			mesh.gameObject.SetActive(false);
			mesh.transform.SetParent(poolContainer, false);
			pool.Enqueue(mesh);
		}

		// Internal Utility: ----------------------------------------------------------------------

		private void PrePool()
		{
			MeshFilter[] t = new MeshFilter[prePoolCount];
			for (int i = prePoolCount - 1; i >= 0; i--) t[i] = GetPoolObject();
			for (int i = t.Length - 1; i >= 0; i--) Recycle(t[i]);
		}

		// ----------------------------------------------------------------------------------------

		public bool ChunkHasMesh(GeometryChunk chunk) => meshMap.ContainsKey(chunk);

		public void AttachMeshToChunk(GeometryChunk chunk)
		{
			if (chunk == null || meshMap.ContainsKey(chunk)) return;

			MeshFilter meshFilter = GetPoolObject();
			Mesh mesh = chunk.SelfCollider.CreateMesh(false, false);
			meshFilter.sharedMesh = mesh;

			// TODO find better workaround to this stupid unity bug that basically makes the mesh
			// generate with the world transform if the chunk has existed for more than one frame,
			// otherwise the mesh is nontransformed
			float xb;
			float yb;

			if (Application.isPlaying)
			{
				xb = meshFilter.mesh.bounds.center.x;
				yb = meshFilter.mesh.bounds.center.y;
			}
			else
			{
				xb = meshFilter.sharedMesh.bounds.center.x;
				yb = meshFilter.sharedMesh.bounds.center.y;
			}

			bool stupidMesh = (
				xb > chunk.transform.position.x && xb < chunk.transform.position.x + MarchingSquares.ChunkSize.x &&
				yb > chunk.transform.position.y && yb < chunk.transform.position.y + MarchingSquares.ChunkSize.y
			);

			if (stupidMesh)
			{
				meshFilter.transform.position = Vector3.zero;
				meshFilter.transform.rotation = Quaternion.identity;
			}
			else
			{
				meshFilter.transform.position = chunk.transform.position;
				meshFilter.transform.rotation = chunk.transform.rotation;
			}

			// mesh.transform.localScale = chunk.transform.lossyScale;
			// mesh.transform.position = Vector3.zero; 
			// mesh.transform.rotation = Quaternion.identity;
			// mesh.transform.localScale = Vector3.one;

			meshFilter.gameObject.SetActive(true);
			meshMap.Add(chunk, meshFilter);
		}

		public void RemoveMeshFromChunk(GeometryChunk chunk)
		{
			if (meshMap.ContainsKey(chunk))
			{
				meshMap.Remove(chunk, out MeshFilter mesh);
				Recycle(mesh);
			}
		}

		public void OnChunkModified(GeometryChunk chunk)
		{
			if (meshMap.ContainsKey(chunk))
			{
				RemoveMeshFromChunk(chunk);
				AttachMeshToChunk(chunk);
			}
		}

		// Unity Messages: ------------------------------------------------------------------------

		private void Awake()
		{
			PrePool();
		}

		private void OnEnable()
		{
			if(cameraChunks != null)
			{
				cameraChunks.OnChunkVisible.AddListener(AttachMeshToChunk);
				cameraChunks.OnChunkInvisible.AddListener(RemoveMeshFromChunk);
				cameraChunks.MarchingSquares.OnGeometryChunkModified.AddListener(OnChunkModified);
			}
		}

		private void OnDisable()
		{
			if(cameraChunks != null)
			{
				cameraChunks.OnChunkVisible.RemoveListener(AttachMeshToChunk);
				cameraChunks.OnChunkInvisible.RemoveListener(RemoveMeshFromChunk);
				cameraChunks.MarchingSquares.OnGeometryChunkModified.RemoveListener(OnChunkModified);
			}
		}
	}
}