using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GeoFlex.Core;

namespace GeoFlex.Utility
{
	[AddComponentMenu("GeoFlex/Camera Chunk Loader")]
	public class CameraChunkLoader : MonoBehaviour
	{
		[SerializeField]
		private readonly GeometryChunkEvent onChunkVisible = new GeometryChunkEvent();

		[SerializeField]
		private readonly GeometryChunkEvent onChunkInvisible = new GeometryChunkEvent();

		[SerializeField]
		private Camera cameraTarget = null;

		[SerializeField]
		private GeometryController marchingSquares = null;

		private RectInt previousRange = new RectInt(99999, 99999, 0, 0);

		// ----------------------------------------------------------------------------------------

		public IListenableEvent<GeometryChunk> OnChunkVisible => onChunkVisible;

		public IListenableEvent<GeometryChunk> OnChunkInvisible => onChunkInvisible;

		public GeometryController MarchingSquares => marchingSquares;

		// ----------------------------------------------------------------------------------------

		[ContextMenu("Update Active Chunks")]
		private void UpdateActiveChunks()
		{
			Vector2 min = cameraTarget.ViewportToWorldPoint(Vector3.zero);
			Vector2 max = cameraTarget.ViewportToWorldPoint(Vector3.one);
			Rect worldRect = Rect.MinMaxRect(min.x, min.y, max.x, max.y);
			RectInt currentRange = marchingSquares.WorldRectToChunkRange(worldRect);

			marchingSquares.SetActiveChunkRange(currentRange);

			// iterate through each newly visible chunk
			for(int x = currentRange.xMin; x <= currentRange.xMax; x++)
			{
				for(int y = currentRange.yMin; y <= currentRange.yMax; y++)
				{
					// skip segments that were previously visible
					if (
						x <= previousRange.xMax && x >= previousRange.xMin &&
						y <= previousRange.yMax && y >= previousRange.yMin
					) {
						// y += previousVisibleRange.yMax - y;
						continue;
					}
					GeometryChunk chunk = marchingSquares.Chunks[x, y];
					onChunkVisible.Invoke(chunk);
				}
			}

			// iterate through each chunk that was visible but no longer is
			for (int x = previousRange.xMin; x <= previousRange.xMax; x++)
			{
				for (int y = previousRange.yMin; y <= previousRange.yMax; y++)
				{
					// skip segments that are currently visible
					if (
						x <= currentRange.xMax && x >= currentRange.xMin &&
						y <= currentRange.yMax && y >= currentRange.yMin
					) {
						// y += currentRange.yMax - y;
						continue;
					}
					GeometryChunk chunk = marchingSquares.Chunks[x, y];
					if(chunk != null) onChunkInvisible.Invoke(chunk);
				}
			}

			previousRange = currentRange;
		}

		// ----------------------------------------------------------------------------------------

		private void Update()
		{
			UpdateActiveChunks();
		}
	}
}