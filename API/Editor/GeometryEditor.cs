using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using GeoFlex.Core;
using System;

namespace GeoFlex.Editor
{
	public class GeometryEditor : EditorWindow
	{
		[MenuItem("Window/Marching Squares/Geometry Editor")]
		public static void Init()
		{
			GeometryEditor editor = GetWindow(typeof(GeometryEditor)) as GeometryEditor;

			editor.SelfInit();
		}

		private static readonly List<GeometryEditor> activeGeometryEditors = new List<GeometryEditor>();

		public static IReadOnlyList<GeometryEditor> ActiveGeometryEditors => activeGeometryEditors;

		public static readonly string UNDO_GROUP_GEOMEDIT = "Geometry Edit";

		// ----------------------------------------------------------------------------------------

		private readonly HashSet<GeometryChunk> keptChunks = new HashSet<GeometryChunk>();

		private readonly HashSet<Vector2Int> selectedIndices = new HashSet<Vector2Int>();

		private bool unityWindowFocused = true;

		private Vector3[] qPath = new Vector3[] { };

		private Camera sceneCam = null;

		private RenderTexture sceneTex = null;

		private Vector2 sceneTexSize = default;

		private Vector2 currentViewPos = default;

		private Vector2 worldMousePress = default;

		private Vector2 windowMousePos = default;

		private Vector2 worldMousePos = default;

		private float currentCamSize = 10;

		private bool selectionAppendMode = false;

		private bool selectionDifferenceMode = false;

		private bool chunkSelecting = false;

		private Vector2 chunkSelectBegin = default;

		// ----------------------------------------------------------------------------------------

		public Camera SceneCam => sceneCam;

		public bool cursorSelectMode = false;

		public bool focusWindowOnHover = true;

		public GeometryController targetGeometry;

		public float brushSize = 4;

		public bool alwaysRenderSelection = false;

		public Color selectedGeometryColor = Color.yellow;

		public Color keptGeometryColor = Color.green;

		public Color brushColor = Color.green;

		public Color selectionColor = Color.green;

		// Input Actions: -------------------------------------------------------------------------

		private void BeginChunkSelection()
		{
			if (chunkSelecting) return;
			chunkSelecting = true;
			chunkSelectBegin.Set(worldMousePos.x, worldMousePos.y);
		}

		private void EndChunkSelection()
		{
			Vector2 min = Vector2.Min(chunkSelectBegin, worldMousePos);
			Vector2 max = Vector2.Max(chunkSelectBegin, worldMousePos);
			Vector2Int minIndex = targetGeometry.WorldPosToChunkIndex(min);
			Vector2Int maxIndex = targetGeometry.WorldPosToChunkIndex(max);

			if(!selectionAppendMode && !selectionDifferenceMode) selectedIndices.Clear();

			for(int x = minIndex.x; x <= maxIndex.x; x++)
			{
				for(int y = minIndex.y; y <= maxIndex.y; y++)
				{
					if (!selectionDifferenceMode) selectedIndices.Add(new Vector2Int(x, y));

					else if (selectedIndices.Contains(new Vector2Int(x, y)))
					{
						selectedIndices.Remove(new Vector2Int(x, y));
					}
				}
			}

			chunkSelecting = false;
		}

		private void DragCameraPan(Vector2 delta)
		{
			currentViewPos += delta;
		}

		private void ZoomIn()
		{
			if(currentCamSize > 1) currentCamSize -= 1;
			else
			{
				currentCamSize /= 2;
			}
		}

		private void ZoomOut()
		{
			if(currentCamSize >= 1) currentCamSize += 1;
			else
			{
				currentCamSize *= 2;
				if (currentCamSize > 1) currentCamSize = 1;
			}
		}

		private void BrushAtMouse()
		{
			targetGeometry.FillRadiusAt(worldMousePos, brushSize);
			EditorUtility.SetDirty(targetGeometry);
		}

		private void EraserAtMouse()
		{
			targetGeometry.EmptyRadiusAt(worldMousePos, brushSize);
			EditorUtility.SetDirty(targetGeometry);
		}

		// Internal Utility: ----------------------------------------------------------------------

		private bool ValidateTargetGeometry()
		{
			if (targetGeometry == null) SelfInit();
			return targetGeometry != null;
		}

		private void CaptureInput(Event ev)
		{
			windowMousePos.Set(Event.current.mousePosition.x, Event.current.mousePosition.y);
			worldMousePos = sceneCam.ScreenToWorldPoint(
				new Vector2(windowMousePos.x, position.height - windowMousePos.y)
			);
			if(focusWindowOnHover) FocusOnHover();

			switch (ev.type)
			{
				case EventType.MouseDown:
					worldMousePress = worldMousePos;
					if (cursorSelectMode)
					{
						if (ev.button == 0) BeginChunkSelection();
					}
					else
					{
						Undo.RegisterCompleteObjectUndo(targetGeometry, UNDO_GROUP_GEOMEDIT);
						if(ev.button == 0) BrushAtMouse();
						if (ev.button == 1) EraserAtMouse();
					}
					break;
				case EventType.MouseDrag:
					Vector2 delta = sceneCam.ScreenToWorldPoint(default) - sceneCam.ScreenToWorldPoint(
						new Vector2(ev.delta.x, -ev.delta.y)
					);
					if (ev.button == 2) DragCameraPan(delta);
					if(!cursorSelectMode)
					{
						if (ev.button == 0) BrushAtMouse();
						if (ev.button == 1) EraserAtMouse();
					}
					break;
				case EventType.MouseUp:
					if (ev.button == 0)
					{
						if (cursorSelectMode) EndChunkSelection();
					}
					break;
				case EventType.KeyDown:
					if (ev.keyCode == KeyCode.LeftShift) cursorSelectMode = true;
					if (ev.keyCode == KeyCode.LeftControl) selectionAppendMode = true;
					if (ev.keyCode == KeyCode.LeftAlt) selectionDifferenceMode = true;
					break;
				case EventType.KeyUp:
					if (ev.keyCode == KeyCode.LeftShift) cursorSelectMode = false;
					if (ev.keyCode == KeyCode.LeftControl) selectionAppendMode = false;
					if (ev.keyCode == KeyCode.LeftAlt) selectionDifferenceMode = false;
					break;
				case EventType.ScrollWheel:
					if (ev.delta.y < 0)
					{
						if (selectionAppendMode) brushSize += 1;
						else ZoomIn();
					}
					else if (ev.delta.y > 0)
					{
						if (selectionAppendMode)
						{
							brushSize -= 1;
							if (brushSize <= 0) brushSize = 0.1f;
						}
						else ZoomOut();
					}
					break;
				case EventType.ValidateCommand:
					if (ev.commandName == "UndoRedoPerformed")
					{
						OnUndo();
					}
					break;
			}
		}

		private void FocusOnHover()
		{
			if (!unityWindowFocused) return;
			bool insideWindow = (
				windowMousePos.x >= 0 && 
				windowMousePos.x < position.width && 
				windowMousePos.y >= 0 && 
				windowMousePos.y < position.height
			);
			if (insideWindow)
			{
				if (focusedWindow != this)
				{
					Focus();
				}
			}
		}

		private void KeepAllLoadedChunks()
		{
			keptChunks.Clear();
			foreach(Transform child in targetGeometry.ChunkContainer.transform)
			{
				GeometryChunk chunk = child.GetComponent<GeometryChunk>();
				if(chunk != null && !keptChunks.Contains(chunk))
				{
					keptChunks.Add(chunk);
				}
			}
		}

		private void CreateCamera()
		{
			sceneCam = new GameObject("Geometry Editor Cam").AddComponent<Camera>();
			sceneCam.gameObject.hideFlags = HideFlags.HideAndDontSave;
			sceneCam.transform.position = new Vector3(currentViewPos.x, currentViewPos.y, -10);
			sceneCam.orthographic = true;
			sceneCam.orthographicSize = currentCamSize;
			sceneCam.enabled = false;

			CreateRenderTexture();
		}

		private void CreateRenderTexture()
		{
			if (sceneTex != null)
			{
				if (sceneCam.targetTexture == sceneTex) sceneCam.targetTexture = null;
				DestroyImmediate(sceneTex);
			}

			sceneTexSize.Set(position.width, position.height);
			sceneTex = new RenderTexture(
				Mathf.FloorToInt(sceneTexSize.x), 
				Mathf.FloorToInt(sceneTexSize.y), 
				24
			);
			sceneCam.targetTexture = sceneTex;
		}

		private void HandleRendering()
		{
			RenderSelectedChunkGeometry();

			if (cursorSelectMode)
			{
				RenderSelectionBox();
				RenderSelectedChunkBounds();
				RenderMouseChunk();
			}

			else if (alwaysRenderSelection) RenderSelectedChunkBounds();
			RenderKeptChunks();

			RenderMouseBrush();
		}

		private void RenderSelectionBox()
		{
			if (chunkSelecting)
			{
				Vector2 boxPos = WorldToWindowPos(chunkSelectBegin);
				Vector2 size = boxPos - windowMousePos;
				Handles.color = selectionColor;
				Handles.DrawWireCube((boxPos + windowMousePos) * 0.5f, size);
			}
		}

		private void RenderMouseChunk()
		{
			Color col = selectionColor;
			col.a = 0.25f;
			Handles.color = col;
			Vector2Int index = targetGeometry.WorldPosToChunkIndex(worldMousePos);
			Vector2 boxPos = WorldToWindowPos(targetGeometry.ChunkIndexToWorldPos(index));
			Vector2 size = WorldToWindowPos(targetGeometry.ChunkGrid.cellSize) - WorldToWindowPos(Vector2.zero);
			Handles.DrawWireCube(boxPos + size * 0.5f, size);
		}

		private void RenderMouseBrush()
		{
			Vector2 dif = (
				WorldToWindowPos(targetGeometry.ChunkGrid.LocalToWorld(new Vector2(brushSize, brushSize))) -
				WorldToWindowPos(targetGeometry.ChunkGrid.LocalToWorld(Vector2.zero))
			);

			Color col = brushColor;
			if (cursorSelectMode) col.a = 0.25f;
			Handles.color = col;

			Handles.DrawWireDisc(windowMousePos, Vector3.back, Math.Max(dif.x, dif.y) * 0.25f);
		}

		private void RenderSelectedChunkBounds()
		{
			Color col = selectionColor;
			col.a = 0.25f;
			Handles.color = col;
			foreach (Vector2Int index in selectedIndices)
			{
				Vector2 boxPos = WorldToWindowPos(targetGeometry.ChunkIndexToWorldPos(index));
				Vector2 size = WorldToWindowPos(targetGeometry.ChunkGrid.cellSize) - WorldToWindowPos(Vector2.zero);
				Handles.DrawWireCube(boxPos + size * 0.5f, size);
			}
		}

		private void RenderSelectedChunkGeometry()
		{
			Handles.color = selectedGeometryColor;
			foreach (Vector2Int index in selectedIndices)
			{
				GeometryChunk chunk = targetGeometry.Chunks.ForceGetGeometryChunk(index.x, index.y);
				if (keptChunks.Contains(chunk)) continue;
				RenderChunkGeometry(chunk);
			}
		}

		private void RenderKeptChunks()
		{
			Handles.color = keptGeometryColor;
			foreach (GeometryChunk chunk in keptChunks)
			{
				RenderChunkGeometry(chunk);
			}
		}

		private void RenderChunkGeometry(GeometryChunk chunk)
		{
			PolygonCollider2D collider = chunk.SelfCollider;
			for(int i = collider.pathCount - 1; i >= 0; i--)
			{
				Vector2[] path = collider.GetPath(i);
				Vector3[] convertedPath = ColliderPathToHandlesPath(path, collider.transform);
				Handles.DrawAAPolyLine(convertedPath);
			}
		}

		private Vector3[] ColliderPathToHandlesPath(in Vector2[] path, Transform pathTransform)
		{
			if(qPath.Length != path.Length) Array.Resize(ref qPath, path.Length);
			for(int i = path.Length - 1; i >= 0; i--)
			{
				Vector3 targ = WorldToWindowPos(pathTransform.TransformPoint(path[i]));
				qPath[i] = targ;
			}

			return qPath;
		}

		private Vector2 WorldToWindowPos(Vector2 worldPos)
		{
			Vector2 r = sceneCam.WorldToScreenPoint(new Vector2(worldPos.x,worldPos.y));
			r.y = position.height - r.y;
			return r;
		}

		// External Utility: ----------------------------------------------------------------------

		public void SelfInit()
		{
			EditorApplication.focusChanged -= UnityFocusChanged;
			EditorApplication.focusChanged += UnityFocusChanged;

			targetGeometry = FindObjectOfType<GeometryController>();
			currentViewPos = targetGeometry.transform.position;

			titleContent.text = "Geometry";

			KeepAllLoadedChunks();
			Show();

			if (!activeGeometryEditors.Contains(this)) activeGeometryEditors.Add(this);
		}

		public void ClearSelection()
		{
			selectedIndices.Clear(); 
		}

		public void KeepSelectedChunks()
		{
			foreach(Vector2Int sel in selectedIndices)
			{
				GeometryChunk chunk = targetGeometry.Chunks[sel.x, sel.y];
				if (keptChunks.Contains(chunk)) continue;
				keptChunks.Add(chunk);
			}
		}

		public void RemoveSelectedChunks()
		{
			foreach(Vector2Int sel in selectedIndices)
			{
				GeometryChunk chunk = targetGeometry.Chunks[sel.x, sel.y];
				if (!keptChunks.Contains(chunk)) continue;
				keptChunks.Remove(chunk);
			}
		}

		public void UnloadUnneededChunks()
		{
			List<GeometryChunk> toDestroy = new List<GeometryChunk>();
			foreach (Transform child in targetGeometry.ChunkContainer.transform)
			{
				GeometryChunk chunk = child.GetComponent<GeometryChunk>();
				if (chunk != null && !keptChunks.Contains(chunk) && !selectedIndices.Contains(chunk.index))
				{
					toDestroy.Add(chunk);
				}
			}

			for (int i = toDestroy.Count - 1; i >= 0; i--)
			{
				targetGeometry.Chunks.DereferenceChunk(toDestroy[i]);
				DestroyImmediate(toDestroy[i].gameObject);
			}
			toDestroy.Clear();
		}

		public void VerifyChunkAndScalarIntegrity()
		{
			foreach(Vector2Int index in selectedIndices)
			{
				GeometryChunk chunk = targetGeometry.Chunks.GetGeometryChunk(index.x, index.y);
				if(chunk == null)
				{
					ClearSelection();
					break;
				}
			}

			foreach (GeometryChunk chunk in keptChunks)
			{
				if(chunk == null)
				{
					// TODO
				}
			}

			List<GeometryChunk> toDestroy = new List<GeometryChunk>();
			foreach (Transform child in targetGeometry.ChunkContainer.transform)
			{
				GeometryChunk chunk = child.GetComponent<GeometryChunk>();
				if (chunk != null)
				{
					if(chunk.scalarField == null)
					{
						toDestroy.Add(chunk);
					}
				}
			}

			for (int i = toDestroy.Count - 1; i >= 0; i--)
			{
				targetGeometry.Chunks.DereferenceChunk(toDestroy[i]);
				DestroyImmediate(toDestroy[i].gameObject);
			}
			toDestroy.Clear();
		}

		// Unity Messages: ------------------------------------------------------------------------

		private void UnityFocusChanged(bool focus)
		{
			unityWindowFocused = focus;
		}

		private void OnUndo()
		{
			if (Undo.GetCurrentGroupName() != UNDO_GROUP_GEOMEDIT) return;
			int group = Undo.GetCurrentGroup();

			Debug.Log("Undo Detected! -" + group);

			// TODO - actually undo geometry edits
		}

		private void OnEnable()
		{
			if (targetGeometry == null)
			{
				targetGeometry = FindObjectOfType<GeometryController>();
			}

			if (sceneCam == null) CreateCamera();
		}

		private void OnDisable()
		{
		}

		private void OnDestroy()
		{
			// remove from active geometry editor list
			activeGeometryEditors.Remove(this);

			// Dispose of objects that were created
			DestroyImmediate(sceneCam.gameObject);
			DestroyImmediate(sceneTex);
			keptChunks.Clear();
		}

		private void OnGUI()
		{
			if (!ValidateTargetGeometry()) return;

			CaptureInput(Event.current);

			// render camera pov onto editor window
			if (sceneCam.targetTexture != null)
			{
				GUI.DrawTexture(
					new Rect(0, 0, sceneTex.width, sceneTex.height), 
					sceneCam.targetTexture, 
					ScaleMode.StretchToFill
				);
			}

			HandleRendering();

			// recreate render texture if the window has been resized
			if (position.width != sceneTexSize.x || position.height != sceneTexSize.y)
			{
				CreateRenderTexture();
			}
		}

		private void OnFocus()
		{
			VerifyChunkAndScalarIntegrity();
		}

		private void OnLostFocus()
		{
			UnloadUnneededChunks();
		}

		private void Update()
		{
			if (!ValidateTargetGeometry()) return;
			
			if (!unityWindowFocused) return;
			if (!activeGeometryEditors.Contains(this)) activeGeometryEditors.Add(this);

			sceneCam.transform.position = new Vector3(currentViewPos.x, currentViewPos.y, -10);
			sceneCam.orthographicSize = currentCamSize;

			sceneCam.Render();
			Repaint();
		}
	}
}
