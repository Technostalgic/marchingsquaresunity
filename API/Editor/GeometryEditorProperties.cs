﻿using UnityEditor;
using UnityEngine;
using GeoFlex.Core;

namespace GeoFlex.Editor
{
	public class GeometryEditorProperties : EditorWindow
	{
		[MenuItem("Window/Marching Squares/Geometry Editor Tools")]
		public static void Init()
		{
			GeometryEditorProperties editor = GetWindow(typeof(GeometryEditorProperties)) as GeometryEditorProperties;
			editor.titleContent.text = "Geometry Tools";

			editor.Show();
		}

		// ----------------------------------------------------------------------------------------

		private Vector2 scrollPosition = Vector2.zero;

		private float maxBrushSize = 6;

		private GeometryEditor TargetEditor => 
			GeometryEditor.ActiveGeometryEditors.Count > 0 ? 
			GeometryEditor.ActiveGeometryEditors[0] : null;

		// ----------------------------------------------------------------------------------------

		private void DrawEditorProperties()
		{
			GeometryEditor editor = TargetEditor;
			if(TargetEditor == null)
			{
				EditorGUILayout.LabelField("Error: No geometry editor found");
				return;
			}

			GUIContent focusOnHoverLabel = new GUIContent();
			focusOnHoverLabel.text = "Focus On Hover";
			focusOnHoverLabel.tooltip = "If true, the geometry window will be focused and begin catpturing input whenever the mouse hovers over it";
			editor.focusWindowOnHover = EditorGUILayout.Toggle(focusOnHoverLabel, editor.focusWindowOnHover);

			GUIContent selectModeLabel = new GUIContent();
			selectModeLabel.text = "Selection Mode";
			selectModeLabel.tooltip = "When in selection mode, the mouse will be used to select chunks instead of modifying geometry";
			editor.cursorSelectMode = EditorGUILayout.Toggle(selectModeLabel, editor.cursorSelectMode);

			EditorGUILayout.Space();

			GUIContent geomSectionLabel = new GUIContent();
			geomSectionLabel.text = "Geometry";
			EditorGUILayout.LabelField(geomSectionLabel, EditorStyles.boldLabel);

			GUIContent geomLabel = new GUIContent();
			geomLabel.text = "Target Geometry";
			geomLabel.tooltip = "The geometry object that is being edited by the geometry editor";
			editor.targetGeometry = (GeometryController)EditorGUILayout.ObjectField(
				geomLabel, editor.targetGeometry, typeof(GeometryController), true
			);

			GUIContent selGeomColorLabel = new GUIContent();
			selGeomColorLabel.text = "Selected Geometry Color";
			selGeomColorLabel.tooltip = "The color of the geometry wireframe of chunks that are selected";
			editor.selectedGeometryColor = EditorGUILayout.ColorField(selGeomColorLabel, editor.selectedGeometryColor);

			GUIContent keptGeomColorLabel = new GUIContent();
			keptGeomColorLabel.text = "Kept Geometry Color";
			keptGeomColorLabel.tooltip = "The color of the geometry wireframe that's been marked as to kept";
			editor.keptGeometryColor = EditorGUILayout.ColorField(keptGeomColorLabel, editor.keptGeometryColor);

			GUIContent bgColorLabel = new GUIContent();
			bgColorLabel.text = "Background Color";
			bgColorLabel.tooltip = "The color of the geometry editor background";
			editor.SceneCam.backgroundColor = EditorGUILayout.ColorField(bgColorLabel, editor.SceneCam.backgroundColor);

			EditorGUILayout.Space();

			GUIContent brushSectionLabel = new GUIContent();
			brushSectionLabel.text = "Brush";
			EditorGUILayout.LabelField(brushSectionLabel, EditorStyles.boldLabel);

			GUIContent maxBrushSizeLabel = new GUIContent();
			maxBrushSizeLabel.text = "Max Brush Size";
			maxBrushSizeLabel.tooltip = "The right hand side value of the brush size slider";
			maxBrushSize = EditorGUILayout.FloatField(maxBrushSizeLabel, maxBrushSize);

			GUIContent brushSizeLabel = new GUIContent();
			brushSizeLabel.text = "Brush Size";
			brushSizeLabel.tooltip = "The size of the brush to modify terrain with";
			float sliderRight = Mathf.Max(maxBrushSize, editor.brushSize);
			editor.brushSize = EditorGUILayout.Slider(brushSizeLabel, editor.brushSize, 0, sliderRight);

			GUIContent brushColorLabel = new GUIContent();
			brushColorLabel.text = "Brush Color";
			brushColorLabel.tooltip = "The color of the brush";
			editor.brushColor = EditorGUILayout.ColorField(brushColorLabel, editor.brushColor);

			EditorGUILayout.Space();

			GUIContent selectionSectionLabel = new GUIContent();
			selectionSectionLabel.text = "Selection";
			EditorGUILayout.LabelField(selectionSectionLabel, EditorStyles.boldLabel);

			GUIContent alwaysRdrSelectionLabel = new GUIContent();
			alwaysRdrSelectionLabel.text = "Always Render Selection";
			alwaysRdrSelectionLabel.tooltip = "If true, the selection will be rendered whether or not editor is in selection mode";
			editor.alwaysRenderSelection = EditorGUILayout.Toggle(alwaysRdrSelectionLabel, editor.alwaysRenderSelection);

			GUIContent selColorLabel = new GUIContent();
			selColorLabel.text = "Selection Color";
			selColorLabel.tooltip = "The color of the selected chunks";
			editor.selectionColor = EditorGUILayout.ColorField(selColorLabel, editor.selectionColor);

			EditorGUILayout.Space();

			EditorGUILayout.BeginHorizontal();

			GUIContent keepGeomButton = new GUIContent();
			keepGeomButton.text = "Save Selection";
			if (GUILayout.Button(keepGeomButton))
			{
				TargetEditor.KeepSelectedChunks();
			}

			GUIContent removeGeomButton = new GUIContent();
			removeGeomButton.text = "Remove Selection";
			if (GUILayout.Button(removeGeomButton))
			{
				TargetEditor.RemoveSelectedChunks();
			}

			EditorGUILayout.EndHorizontal(); 

			GUIContent removeUnkeptGeomButton = new GUIContent();
			removeUnkeptGeomButton.text = "Unload Unsaved Chunks";
			if (GUILayout.Button(removeUnkeptGeomButton))
			{
				TargetEditor.ClearSelection();
				TargetEditor.UnloadUnneededChunks();
			}

			EditorGUILayout.Space();

			GUIContent reinitializeButton = new GUIContent();
			reinitializeButton.text = "Reinitialize Editor";
			if (GUILayout.Button(reinitializeButton))
			{
				TargetEditor.SelfInit();
				TargetEditor.VerifyChunkAndScalarIntegrity();
			}
		}

		// ----------------------------------------------------------------------------------------

		private void OnGUI()
		{
			EditorGUILayout.BeginScrollView(scrollPosition);

			EditorGUILayout.LabelField("Geometry Editor Properties", EditorStyles.largeLabel);
			EditorGUILayout.Space();

			DrawEditorProperties();

			EditorGUILayout.EndScrollView();
		}
	}
}