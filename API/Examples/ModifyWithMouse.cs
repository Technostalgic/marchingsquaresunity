using UnityEngine;
using GeoFlex.Core;

namespace GeoFlex
{
	[AddComponentMenu("GeoFlex/Examples/Modify With Mouse")]
	public class ModifyWithMouse : MonoBehaviour
	{
		[SerializeField]
		private Camera cameraTarget = null;

		[SerializeField]
		private GeometryController geometryTarget = null;

		[Range(1, 6)]
		public float brushSize = 2;

		// ----------------------------------------------------------------------------------------

		private void Update()
		{
			Vector2 holePos = cameraTarget.ScreenToWorldPoint(Input.mousePosition);

			if (Input.GetMouseButton(0))
			{
				geometryTarget.FillRadiusAt(holePos, brushSize);
			}
			else if (Input.GetMouseButton(1))
			{
				geometryTarget.EmptyRadiusAt(holePos, brushSize);
			}
		}
	}
}