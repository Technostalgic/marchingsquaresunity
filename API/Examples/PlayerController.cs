using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[AddComponentMenu("GeoFlex/Examples/Player Controller")]
public class PlayerController : MonoBehaviour
{
	private Vector2 moveDir = Vector2.zero;

	private bool isJumping = false;

	private bool isJumpSustaining = false;

	private bool applyGroundFriction = false;

	private bool isTouchingGround = false;

	private Collider2D ground = null;

	private Vector2 groundNormal = default;

	[SerializeField]
	private KeyCode control_left = KeyCode.A;

	[SerializeField]
	private KeyCode control_right = KeyCode.D;

	[SerializeField]
	private KeyCode control_jump = KeyCode.Space;

	private Rigidbody2D physicsBody = null;

	[SerializeField]
	private float jumpStrength = 10;

	[SerializeField, Range(0, 1)]
	private float jumpSustain = 0.25f;

	[SerializeField]
	private float speed = 15;

	[SerializeField]
	public float acceleration = 50;

	[SerializeField, Range(-1, 1)]
	[Tooltip("The shallowness of a slope at which it can be considered ground: 1 is perfectly flat, while 0 is vertical drop")]
	private float groundThreshold = 0.5f;

	private float gravityScale = 0f;

	private float airGroundTimer = 0f;

	// --------------------------------------------------------------------------------------------

	public Rigidbody2D PhysicsBody
	{
		get
		{
			if (physicsBody == null) physicsBody = GetComponent<Rigidbody2D>();
			return physicsBody;
		}
	}

	public Vector2 Velocity
	{
		get => PhysicsBody.velocity;
		set => PhysicsBody.velocity = value;
	}

	public bool IsGrounded => airGroundTimer > 0;

	public Vector2 GroundVelocity
	{
		get
		{
			return (
				(!IsGrounded || ground?.attachedRigidbody == null) ? 
					Vector2.zero : 
					ground.attachedRigidbody.velocity
				);
		}
	}

	// --------------------------------------------------------------------------------------------

	private void HandleInput()
	{
		moveDir = Vector2.zero;
		if (Input.GetKey(control_left)) moveDir.x -= 1;
		if (Input.GetKey(control_right)) moveDir.x += 1;

		if (Input.GetKey(control_jump))
		{
			if(Input.GetKeyDown(control_jump)) isJumping = true;
			isJumpSustaining = true;
		}
		else isJumpSustaining = false; 
	}

	private void HandleMovement()
	{
		HandleHorizontalMovement();
		HandleJumping();
	}

	private void HandlePhysics()
	{
		if(isTouchingGround)
		{
			airGroundTimer = 0.2f;
		}
		else
		{
			Velocity = Velocity += Physics2D.gravity * gravityScale * Time.deltaTime;
		}

		// on ground
		if (IsGrounded)
		{
			if (applyGroundFriction)
			{
				float frictionCeofficient = 0.75f;
				float staticFrictionThreshold = 0.15f;

				Vector2 finalVel = Velocity;
				Vector2 groundVel = GroundVelocity;
				finalVel -= groundVel;
				if (finalVel.magnitude <= staticFrictionThreshold) finalVel.Set(0, 0);
				finalVel *= frictionCeofficient;
				finalVel += groundVel;
				Velocity = finalVel;
			}
		}
	}

	private void HandleHorizontalMovement()
	{
		float targetVel = moveDir.x * speed;
		float deltaVel = 0;
		if (Mathf.Abs(moveDir.x) >= 0.1) 
			deltaVel = Mathf.Sign(targetVel) * acceleration * Time.deltaTime; ;

		float curVel = Velocity.x;
		float finalXVel = curVel;
		bool opposingMovement = MathF.Sign(curVel) != MathF.Sign(targetVel);
		bool isLessThanFullSpeed = Mathf.Abs(curVel) < Mathf.Abs(targetVel);

		// calculate desired horizontal velocity
		if (opposingMovement || isLessThanFullSpeed)
		{
			// apply delta velocity
			finalXVel += deltaVel;

			// enforce max speed
			if (isLessThanFullSpeed && Mathf.Abs(finalXVel) > Mathf.Abs(targetVel))
			{
				finalXVel = targetVel;
			}
		}

		if (!opposingMovement)
		{
			applyGroundFriction = false;
		}

		// apply velocity
		Velocity = new Vector2(finalXVel, Velocity.y);
	}

	private void HandleCollision(Collision2D col)
	{
		// find average point and normal vector
		Vector2 point = default;
		Vector2 normal = default;
		foreach (ContactPoint2D contact in col.contacts)
		{
			point += contact.point;
			normal += contact.normal;
		}
		point /= col.contacts.Length;
		normal /= col.contacts.Length;
		normal.Normalize();

		// check for ground
		CheckForGround(col.collider, point, normal);
	}

	private void CheckForGround(Collider2D collider, in Vector2 point, in Vector2 normal)
	{
		float flatness = Vector2.Dot(normal, -Physics2D.gravity.normalized);
		if(flatness >= groundThreshold)
		{
			isTouchingGround = true;
			ground = collider;
			groundNormal = normal;
		}
	}

	private void HandleJumping()
	{
		if (isJumping)
		{
			Jump();
			isJumping = false;
		}
		else if (isJumpSustaining)
		{
			if (Vector3.Dot(PhysicsBody.velocity, Physics2D.gravity) < 0)
			{
				PhysicsBody.velocity += Physics2D.gravity * -jumpSustain * Time.deltaTime;
			}
			isTouchingGround = false;
			airGroundTimer = 0;
		}
	}

	private void Jump()
	{
		if (airGroundTimer <= 0 && !IsGrounded) return;

		Velocity = new Vector2(Velocity.x, jumpStrength + GroundVelocity.y);

		airGroundTimer = 0;
		isTouchingGround = false;
	}

	// --------------------------------------------------------------------------------------------

	private void Awake()
	{
		gravityScale = PhysicsBody.gravityScale;
		PhysicsBody.gravityScale = 0;
	}

	private void FixedUpdate()
	{
		applyGroundFriction = IsGrounded;
		HandleMovement();
		HandlePhysics();

		if (physicsBody.IsAwake())
		{
			isTouchingGround = false;
			airGroundTimer -= Time.deltaTime;
		}
	}

	private void Update()
	{
		HandleInput();
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		HandleCollision(collision);
	}

	private void OnCollisionStay2D(Collision2D collision)
	{
		HandleCollision(collision);
	}
}
