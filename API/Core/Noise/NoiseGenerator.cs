﻿using System.Collections;
using UnityEngine;

namespace GeoFlex.Core
{
	public abstract class NoiseGenerator : ScriptableObject
	{
		private static readonly System.Random random = new System.Random();

		public static float SeededRandom(float seed)
		{
			Random.InitState((int)(seed * 10000));
			return Random.value;
		}

		[SerializeField, Min(0), Tooltip("Inverse noise scale, smaller numbers produce larger shapes")]
		protected float noiseFrequency = 0.1f;

		[SerializeField, Range(-1, 1), Tooltip("Increase overall noise value by this much")]
		protected float additive = 0;

		[SerializeField, Tooltip("Invert the noise values")]
		protected bool invert = false;

		protected abstract float GenerateValueAt(float x, float y);

		protected float NonClampedValueAt(float x, float y)
		{
			x *= noiseFrequency;
			y *= noiseFrequency;
			float r = GenerateValueAt(x, y);
			if (invert) r = 1 - r;
			r += additive;
			return r;
		}

		public float ValueAt(float x, float y) => Mathf.Clamp01(NonClampedValueAt(x, y));
	}
}