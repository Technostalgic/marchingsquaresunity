using System.Collections;
using UnityEngine;

namespace GeoFlex.Core
{
	[CreateAssetMenu(fileName = "PerlinNoise", menuName = "GeoFlex/Noise/Perlin Noise")]
	public class PerlinNoise : NoiseGenerator
	{
		[SerializeField, Min(1), Tooltip("The amount of octaves that the perlin noise has")]
		protected int octaves = 1;

		[SerializeField, Range(0, 1)]
		[Tooltip("The amount of amplitude and scale decrease for each octave iteration")]
		protected float octaveFalloff = 0.25f;

		protected override float GenerateValueAt(float x, float y)
		{
			float val = 0;
			float octaveValue = 1;
			float octaveTotal = 0;

			for(int i = octaves; i > 0; i--)
			{
				val += Mathf.PerlinNoise(x / octaveValue, y / octaveValue) * octaveValue;
				octaveTotal += octaveValue;
				octaveValue *= 0.5f;
			}

			val /= octaveTotal;

			return val;
		}
	}
}