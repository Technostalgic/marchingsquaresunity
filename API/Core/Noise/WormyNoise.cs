using System.Collections;
using UnityEngine;

namespace GeoFlex.Core
{
	[CreateAssetMenu(fileName = "WormyNoise", menuName = "GeoFlex/Noise/Wormy Noise")]
	public class WormyNoise : NoiseGenerator
	{
		private static Vector2 VectorFromDirection(float direction)
		{
			return new Vector2(
				Mathf.Cos(direction),
				Mathf.Sin(direction)
			);
		}

		protected override float GenerateValueAt(float x, float y)
		{
			float rsize = 1000;

			float minX = Mathf.Floor(x);
			float maxX = Mathf.Ceil(x);

			float minY = Mathf.Floor(y);
			float maxY = Mathf.Ceil(y);

			Vector2 vecTL = VectorFromDirection(SeededRandom(minX * rsize + minY) * Mathf.PI * 2);
			Vector2 vecTR = VectorFromDirection(SeededRandom(maxX * rsize + minY) * Mathf.PI * 2);
			Vector2 vecBL = VectorFromDirection(SeededRandom(minX * rsize + maxY) * Mathf.PI * 2);
			Vector2 vecBR = VectorFromDirection(SeededRandom(maxX * rsize + maxY) * Mathf.PI * 2);

			float xDif = x - minX;
			Vector2 vecT = Vector2.Lerp(vecTL, vecTR, xDif);
			Vector2 vecB = Vector2.Lerp(vecBL, vecBR, xDif);
			Vector2 target = Vector2.Lerp(vecT, vecB, y - minY);
			float r = target.magnitude;

			return r;
		}
	}
}