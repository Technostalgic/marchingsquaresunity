using UnityEngine;
using UnityEngine.Events;

namespace GeoFlex.Core
{
	// Interface Definitions: ---------------------------------------------------------------------

	public interface IListenableEvent<T>
	{
		public void AddListener(UnityAction<T> listener);
		public void RemoveListener(UnityAction<T> listener);
		public void RemoveAllListeners();
	}

	public interface IInvokableEvent<T> : IListenableEvent<T> 
	{
		public void Invoke(T arg0);
	}

	public interface IListenableEvent<T0, T1>
	{
		public void AddListener(UnityAction<T0, T1> listener);
		public void RemoveListener(UnityAction<T0, T1> listener);
		public void RemoveAllListeners();
	}

	public interface IInvokableEvent<T0, T1> : IListenableEvent<T0, T1>
	{
		public void Invoke(T0 arg0, T1 arg1);
	}

	// Serializable Definitions and Interface Implementations: ------------------------------------

	[System.Serializable]
	public class GeometryChunkEvent : UnityEvent<GeometryChunk>, IInvokableEvent<GeometryChunk> { }

	[System.Serializable]
	public class ScalarFieldEvent : UnityEvent<ScalarField>, IInvokableEvent<ScalarField> { }
}