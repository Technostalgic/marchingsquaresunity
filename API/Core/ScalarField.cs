using System.Collections.Generic;
using UnityEngine;

namespace GeoFlex.Core
{
	[System.Serializable]
	public class ScalarField
	{
		private static readonly Queue<ScalarField> scalarFieldPool = new Queue<ScalarField>();

		public static ScalarField Get(int width, int height)
		{
			ScalarField r;

			// TODO ensure width/height when depooling from queue

			if (scalarFieldPool.Count > 0) r = scalarFieldPool.Dequeue();
			else r = new ScalarField(width, height);

			return r;
		}

		public static void RecycleScalarField(ScalarField field)
		{
			scalarFieldPool.Enqueue(field);
		}

		private static Vector2 VectorFromDirection(float direction)
		{
			return new Vector2(
				Mathf.Cos(direction),
				Mathf.Sin(direction)
			);
		}

		/// <summary>
		/// calculate a random number between 0 and 1 based on a seed
		/// </summary>
		public static float SeededRandom(float seed)
		{
			int iseed = (int)Mathf.Floor((float)seed * 100000);

			// idk some bit magic shit that I found online
			int t = iseed += 0x6D2B79F5;
			t = (int)((long)t ^ (long)(uint)t >> 15) * (t | 1);
			t ^= t + ((int)((long)t ^ (long)(uint)t >> 7) * (t | 61));

			float value = ((t ^ (t >> 14)) & 0xFFFFFFFF) / 4294967296f;
			return value;
		}

		/// <summary>
		/// just some smooth noise algorithm I made up, it's not really relevant to anything so 
		/// I won't explain it too much, but it's similar to perlin noise just not as good. Also not 
		/// very performant - not meant for use outside of loading or initialization sequences
		/// </summary>
		/// <param name="x"> x the x-offset for the noise value </param>
		/// <param name="y"> y the y-offset for the noise value </param>
		public static float Noise(float x, float y)
		{
			float rsize = 1000;

			float minX = Mathf.Floor(x);
			float maxX = Mathf.Ceil(x);

			float minY = Mathf.Floor(y);
			float maxY = Mathf.Ceil(y);

			Vector2 vecTL = VectorFromDirection(SeededRandom(minX * rsize + minY) * Mathf.PI * 2);
			Vector2 vecTR = VectorFromDirection(SeededRandom(maxX * rsize + minY) * Mathf.PI * 2);
			Vector2 vecBL = VectorFromDirection(SeededRandom(minX * rsize + maxY) * Mathf.PI * 2);
			Vector2 vecBR = VectorFromDirection(SeededRandom(maxX * rsize + maxY) * Mathf.PI * 2);

			float xDif = x - minX;
			Vector2 vecT = Vector2.Lerp(vecTL, vecTR, xDif);
			Vector2 vecB = Vector2.Lerp(vecBL, vecBR, xDif);
			Vector2 target = Vector2.Lerp(vecT, vecB, y - minY);
			float r = target.magnitude;

			r = 1 - r * r;
			return r * 1.15f;
		}

		private ScalarField(int width, int height)
		{
			fieldData = new float[width * height];
			Width = width;
			Height = height;
		}

		[SerializeField, HideInInspector]
		private float[] fieldData = null;

		public float this[int x, int y]
		{
			get => fieldData[y * Width + x];
			set => fieldData[y * Width + x] = value;
		}

		[field: SerializeField, HideInInspector]
		public int Width { get; private set; } = 0;

		[field: SerializeField, HideInInspector]
		public int Height { get; private set; } = 0;

		public IReadOnlyList<float> FieldData => fieldData;
	}
}