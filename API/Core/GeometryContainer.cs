﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoFlex.Core
{
	public class GeometryContainer
	{
		public GeometryContainer(GeometryController marchingSquares)
		{
			this.marchingSquares = marchingSquares;
			marchingSquares.ChunkPoolContainer.gameObject.SetActive(false);
			GetChunksFromContainer(marchingSquares.ChunkContainer);
		}

		// ----------------------------------------------------------------------------------------

		private readonly Queue<GeometryChunk> chunkPool = new Queue<GeometryChunk>();

		[SerializeField]
		private readonly GeometryChunkEvent onGeometryModified = new GeometryChunkEvent();

		[SerializeField]
		private readonly GeometryChunkEvent onChunkCreated = new GeometryChunkEvent();

		[SerializeField]
		private readonly ScalarFieldEvent onScalarsChanged = new ScalarFieldEvent();

		private GeometryController marchingSquares;

		private Dictionary<(int, int), GeometryChunk> chunkData =
			new Dictionary<(int, int), GeometryChunk>();

		private Dictionary<(int, int), ScalarField> scalarData =
			new Dictionary<(int, int), ScalarField>();

		private RectInt activeChunkRange = new RectInt();

		private RectInt activeScalarRange = new RectInt(
			int.MinValue / 2, int.MinValue / 2, int.MaxValue, int.MaxValue
		);

		// ----------------------------------------------------------------------------------------

		public IListenableEvent<GeometryChunk> OnGeometryModified => OnGeometryModified;

		public IListenableEvent<GeometryChunk> OnChunkCreated => onChunkCreated;

		public IListenableEvent<ScalarField> OnScalarsChanged => onScalarsChanged;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		public GeometryChunk this[int x, int y]
		{
			get => GetGeometryChunk(x, y);
		}

		// Internal Utility: ----------------------------------------------------------------------

		private GeometryChunk GetNewChunk(int indexX, int indexY)
		{
			GeometryChunk r;

			if (chunkPool.Count > 0) r = chunkPool.Dequeue();
			else r = new GameObject("chunk").AddComponent<GeometryChunk>();

			r.gameObject.SetActive(true);
			r.transform.SetParent(marchingSquares.ChunkContainer, false);
			r.transform.localScale = new Vector3(
				marchingSquares.ChunkSize.x, marchingSquares.ChunkSize.y, 1
			);
			r.index.Set(indexX, indexY);
			return r;
		}

		private void GetChunksFromContainer(Transform container)
		{
			foreach (Transform child in container)
			{
				GeometryChunk chunk = child.GetComponent<GeometryChunk>();
				if(chunk != null)
				{
					chunkData.Add((chunk.index.x, chunk.index.y), chunk);
					scalarData.Add((chunk.index.x, chunk.index.y), chunk.scalarField);
					child.position = marchingSquares.ChunkIndexToWorldPos(chunk.index);
				}
			}
		}

		private ScalarField GenerateScalarField(int x, int y)
		{
			var r = ScalarField.Get(
				marchingSquares.ChunkResolution.x, 
				marchingSquares.ChunkResolution.y
			);

			float noiseOffset = 0; //Random.Range(-999999f, 999999f);

			for(int dx = marchingSquares.ChunkResolution.x - 1; dx >= 0; dx--)
			{
				for(int dy = marchingSquares.ChunkResolution.y - 1; dy >= 0; dy--)
				{
					// r[dx, dy] = Random.value;
					float value = marchingSquares.Noise.ValueAt(
						(x * r.Width + dx) + noiseOffset,
						(y * r.Height + dy) + noiseOffset
					);
					r[dx, dy] = value;
				}
			}

			return r;
		}

		public GeometryChunk CreateChunkAt(int x, int y)
		{
			ScalarField scalars = ForceGetScalarField(x, y);
			ScalarField scalarsT = ForceGetScalarField(x, y + 1);
			ScalarField scalarsR = ForceGetScalarField(x + 1, y);
			ScalarField scalarsTR = ForceGetScalarField(x + 1, y + 1);

			GeometryChunk chunk = GetNewChunk(x, y);
			chunk.scalarField = scalars;
			chunk.transform.position = new Vector2(
				x * marchingSquares.ChunkSize.x, y * marchingSquares.ChunkSize.y
			) + (Vector2)marchingSquares.transform.position;
			chunk.UpdateCollisionMesh(scalarsR, scalarsT, scalarsTR);

			chunkData[(x, y)] = chunk;

			return chunk;
		}

		// Primary Functionality: -----------------------------------------------------------------

		/// <summary>
		/// removes all references to chunk and deparents it, use this if you're about to destroy
		/// the chunk for some reason, although most of the time you should probably use 
		/// RecycleChunk()
		/// </summary>
		/// <param name="chunk">the chunk to dereference</param>
		public void DereferenceChunk(GeometryChunk chunk, Transform newParent = null)
		{
			if (scalarData.ContainsKey((chunk.index.x, chunk.index.y)))
			{
				scalarData.Remove((chunk.index.x, chunk.index.y));
			}
			if(chunkData.ContainsKey((chunk.index.x, chunk.index.y)))
			{
				chunkData.Remove((chunk.index.x, chunk.index.y));
			}

			chunk.scalarField = null;
			chunk.gameObject.SetActive(false);
			chunk.transform.SetParent(newParent, false);
		}

		/// <summary>
		/// Recycle a geometry chunk that can be reused later without allocating new memory. After
		/// chunk is recycled, it should NOT be referenced AT ALL. Set all references of it to null
		/// </summary>
		/// <param name="chunk">which chunk to flag for recycling</param>
		public void RecycleChunk(GeometryChunk chunk)
		{
			chunk.OnRecycled.Invoke(chunk);
			chunk.OnRecycled.RemoveAllListeners();

			ScalarField.RecycleScalarField(chunk.scalarField);
			DereferenceChunk(chunk, marchingSquares.ChunkPoolContainer);
		}

		public float GetScalarValue(int x, int y)
		{
			float divX = 1f / (marchingSquares.ChunkResolution.x);
			float divY = 1f / (marchingSquares.ChunkResolution.y);
			int sx = Mathf.FloorToInt(x * divX);
			int sy = Mathf.FloorToInt(y * divY);

			ScalarField scalars = GetScalarField(sx, sy);
			scalars ??= GenerateScalarField(sx, sy);
			try
			{
				return scalars[
					x - (sx * marchingSquares.ChunkResolution.x), 
					y - (sy * marchingSquares.ChunkResolution.y)
				];
			}
			catch
			{
				Debug.LogError("This should not be happening");
				return 0;
			}
		}

		public void SetScalarValue(int x, int y, float value)
		{
			float divX = 1f / (marchingSquares.ChunkResolution.x);
			float divY = 1f / (marchingSquares.ChunkResolution.y);
			int sx = Mathf.FloorToInt(x * divX);
			int sy = Mathf.FloorToInt(y * divY);

			ScalarField scalars = GetScalarField(sx, sy);
			scalars ??= GenerateScalarField(sx, sy);

			int ix = x - (sx * marchingSquares.ChunkResolution.x);
			int iy = y - (sy * marchingSquares.ChunkResolution.y);
			scalars[ix, iy] = value;
		}

		public ScalarField GetScalarField(int x, int y)
		{
			if (!scalarData.ContainsKey((x, y))) return null;
			return scalarData[(x, y)];
		}

		public ScalarField ForceGetScalarField(int x, int y)
		{
			ScalarField r = GetScalarField(x, y);
			if (r == null)
			{
				r = GenerateScalarField(x, y);
				scalarData[(x, y)] = r;
			}

			return r;
		}

		public GeometryChunk GetGeometryChunk(int x, int y)
		{
			if (!chunkData.ContainsKey((x, y))) return null;
			return chunkData[(x, y)];
		}

		public GeometryChunk ForceGetGeometryChunk(int x, int y)
		{
			GeometryChunk r = this[x, y];
			if (r == null) r = CreateChunkAt(x, y);

			return r;
		}

		/// <summary>
		/// Set the range of indices which correspond to which chunks should be active
		/// </summary>
		/// <returns> 
		/// 	a bool indicating whether or not the range changed at all from it's 
		/// 	previous value
		/// </returns>
		public bool SetActiveChunkRange(int minX, int minY, int maxX, int maxY)
		{
			bool r = ( 
				activeChunkRange.x != minX || activeChunkRange.y != minY ||
				activeChunkRange.xMax != maxX || activeChunkRange.yMax != maxY
			);

			activeChunkRange.x = minX;
			activeChunkRange.y = minY;
			activeChunkRange.width = maxX - minX;
			activeChunkRange.height = maxY - minY;

			return r;
		}

		/// <summary>
		/// ensure all the chunks within the active range are loaded and active
		/// </summary>
		public void CreateActiveChunks()
		{
			for (int x = activeChunkRange.width; x >= 0; x--)
			{
				int dx = x + activeChunkRange.x;
				for (int y = activeChunkRange.height; y >= 0; y--)
				{
					int dy = y + activeChunkRange.y;

					GeometryChunk chunk = this[dx, dy];
					if (chunk == null) CreateChunkAt(dx, dy);
				}
			}
		}
	}
}