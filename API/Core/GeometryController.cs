using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GeoFlex.Core
{
	[AddComponentMenu("GeoFlex/Geometry Controller")]
	public class GeometryController : MonoBehaviour
	{
		// ----------------------------------------------------------------------------------------

		private readonly GeometryChunkEvent onGeometryChunkModified = new GeometryChunkEvent();

		private readonly HashSet<GeometryChunk> qGeometryChunkSet = new HashSet<GeometryChunk>();

		private GeometryContainer chunks = null;

		[SerializeField]
		private Vector2Int chunkResolution = new Vector2Int(16, 16);

		[SerializeField]
		private Grid chunkGrid = null;

		[SerializeField]
		private Transform chunkContainer = null;

		[SerializeField]
		private Transform chunkPoolContainer = null;

		[SerializeField]
		private NoiseGenerator noise = null;

		// Exposed Properties: --------------------------------------------------------------------

		public Vector2Int ChunkResolution => chunkResolution;

		public IListenableEvent<GeometryChunk> OnGeometryChunkModified => onGeometryChunkModified;

		public GeometryContainer Chunks 
		{
			get
			{
				if (chunks == null) chunks = new GeometryContainer(this);
				return chunks;
			}
		}

		public Grid ChunkGrid => chunkGrid;

		public Vector3 ChunkSize => chunkGrid.cellSize;

		public Transform ChunkContainer
		{
			get
			{
				if (chunkContainer == null)
				{
					chunkContainer = new GameObject("Chunks").transform;
					chunkContainer.SetParent(transform, false);
				}
				return chunkContainer;
			}
		}

		public Transform ChunkPoolContainer
		{
			get
			{
				if(chunkPoolContainer == null)
				{
					chunkPoolContainer = new GameObject("Chunk Pool").transform;
					chunkPoolContainer.gameObject.SetActive(false);
					chunkPoolContainer.SetParent(transform, false);
				}
				return chunkPoolContainer;
			}
		}

		public NoiseGenerator Noise => noise;

		// Internal Utility: ----------------------------------------------------------------------

		[ContextMenu("Test Chunk Generation with chunk 0")]
		private void TestChunk0()
		{
			Chunks.CreateChunkAt(0, 0);
		}

		public Vector2 WorldPosToScalarSpace(in Vector2 pos)
		{
			Vector2 tpos = chunkGrid.WorldToLocal(pos);
			tpos.x *= chunkResolution.x / chunkGrid.cellSize.x;
			tpos.y *= chunkResolution.y / chunkGrid.cellSize.y;
			return tpos;
		}

		// Geometry Modification: -----------------------------------------------------------------

		public void EmptyRadiusAt(in Vector2 pos, float radius)
		{
			qGeometryChunkSet.Clear();

			// TODO convert radius to scalar space

			Vector2 tpos = WorldPosToScalarSpace(pos);
			int minX = Mathf.FloorToInt(tpos.x - radius);
			int minY = Mathf.CeilToInt(tpos.y - radius - 1);
			int maxX = Mathf.FloorToInt(tpos.x + radius + 1); 
			int maxY = Mathf.CeilToInt(tpos.y + radius);
			float distFactor = 1 / radius * 0.5f;

			Vector2 xy = default;
			for(int x = minX; x <= maxX; x++)
			{
				for(int y = minY; y <= maxY; y++)
				{
					xy.Set(x, y);
					float minVal = Vector2.Distance(tpos, xy) * distFactor;
					float val = Chunks.GetScalarValue(x, y);
					Chunks.SetScalarValue(x, y, Mathf.Min(minVal, val));

					GeometryChunk chunk = Chunks.GetGeometryChunk(
						Mathf.FloorToInt(x / (float)chunkResolution.x), Mathf.FloorToInt(y / (float)chunkResolution.y)
					);
					if (chunk != null) qGeometryChunkSet.Add(chunk);
				}
			}

			foreach (GeometryChunk chunk in qGeometryChunkSet)
			{
				ScalarField nr = Chunks.ForceGetScalarField(chunk.index.x + 1, chunk.index.y);
				ScalarField nb = Chunks.ForceGetScalarField(chunk.index.x, chunk.index.y + 1);
				ScalarField nbr = Chunks.ForceGetScalarField(chunk.index.x + 1, chunk.index.y + 1);
				chunk.UpdateCollisionMesh(nr, nb, nbr);
				onGeometryChunkModified.Invoke(chunk);
			}
		}
		
		public void FillRadiusAt(in Vector2 pos, float radius)
		{
			qGeometryChunkSet.Clear();

			// TODO convert radius to scalar space

			Vector2 tpos = WorldPosToScalarSpace(pos);
			int minX = Mathf.FloorToInt(tpos.x - radius);
			int minY = Mathf.CeilToInt(tpos.y - radius - 1);
			int maxX = Mathf.FloorToInt(tpos.x + radius + 1);
			int maxY = Mathf.CeilToInt(tpos.y + radius);
			float distFactor = 1 / radius * 0.5f;

			Vector2 xy = default;
			for (int x = minX; x <= maxX; x++)
			{
				for (int y = minY; y <= maxY; y++)
				{
					xy.Set(x, y);
					float maxVal = 1 - Vector2.Distance(tpos, xy) * distFactor;
					float val = Chunks.GetScalarValue(x, y);
					Chunks.SetScalarValue(x, y, Mathf.Max(maxVal, val));

					GeometryChunk chunk = Chunks.GetGeometryChunk(
						Mathf.FloorToInt(x / (float)chunkResolution.x), Mathf.FloorToInt(y / (float)chunkResolution.y)
					);
					if (chunk != null) qGeometryChunkSet.Add(chunk);
				}
			}

			foreach (GeometryChunk chunk in qGeometryChunkSet)
			{
				ScalarField nr = Chunks.ForceGetScalarField(chunk.index.x + 1, chunk.index.y);
				ScalarField nb = Chunks.ForceGetScalarField(chunk.index.x, chunk.index.y + 1);
				ScalarField nbr = Chunks.ForceGetScalarField(chunk.index.x + 1, chunk.index.y + 1);
				chunk.UpdateCollisionMesh(nr, nb, nbr);
				onGeometryChunkModified.Invoke(chunk);
			}
		}

		// Primary Funcitonality: -----------------------------------------------------------------

		public Vector2 WorldPosToChunkSpace(in Vector2 pos)
		{
			return chunkGrid.WorldToLocal(pos);
		}

		public Vector2Int WorldPosToChunkIndex(in Vector2 pos)
		{
			Vector3Int v3i = chunkGrid.WorldToCell(pos);
			return new Vector2Int(v3i.x, v3i.y);
		}

		public Vector2 ChunkIndexToWorldPos(in Vector2Int index)
		{
			return chunkGrid.CellToWorld(new Vector3Int(index.x, index.y));
		}

		public RectInt WorldRectToChunkRange(in Rect worldRect)
		{
			Vector3Int min = chunkGrid.WorldToCell(worldRect.min);
			Vector3Int max = chunkGrid.WorldToCell(worldRect.max);

			return new RectInt(min.x, min.y, max.x - min.x, max.y - min.y);
		}

		/// <summary>
		/// Force all the chunks within the specifed range to be loaded and active
		/// </summary>
		public void SetActiveChunkRange(in RectInt range)
		{
			bool changed = Chunks.SetActiveChunkRange(range.xMin, range.yMin, range.xMax, range.yMax);
			if (changed) chunks.CreateActiveChunks();
		}

		// ----------------------------------------------------------------------------------------

		private void Start()
		{
			chunks = Chunks;
		}
	}
}
